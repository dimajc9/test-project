///// Two Sum /////

let nums = [3,2,4] 
let target = 6

const sum = (nums, target) => {
    for(let i=0; i<nums.length; i++){
        for(let j=i+1; j<nums.length; j++){
        if(( nums[i] + nums[j] ) === target){
            return [i, j]
        }
      }
    }
}

console.log(sum(nums, target));

///// 2. Integer to Roman /////

const convertRoman = (num) => {
    let roman = ''

    for(let i = -1; i < num; i++){
        if(!(num % 1000)){ roman += 'M', num -= 1000 }
        else if(!(num % 500)){ roman += 'D', num -= 500 }
        else if(!(num % 100)){ roman += 'C', num -= 100 }
        else if(!(num % 50)){ roman += 'L', num -= 50 }
        else if(!(num % 10)){ roman += 'X', num -= 10 }
        else if(!(num % 5)){ roman += 'V', num -= 5 }
        else if(num === 4){ roman += 'VI', num -= 4 }
        else if(num === 3){ roman += 'III', num -= 3 }
        else if(!(num % 1)){ roman += 'I', num -= 1 }
    }

    roman = roman.split('').reverse().join('')

    let arr = {
        DCCCC: 'CM',
        CCCC: 'CD',
        LXXXX: 'XC',
        XXXX: 'XL',
        VIIII: 'IX',
        IIII: 'IV',
    }

    for(let i in arr){
        roman = roman.replace(new RegExp(i,"g"), arr[i])
    }

    return roman
} 

console.log(convertRoman(3))
console.log(convertRoman(4))
console.log(convertRoman(9))
console.log(convertRoman(58))
console.log(convertRoman(1994))
